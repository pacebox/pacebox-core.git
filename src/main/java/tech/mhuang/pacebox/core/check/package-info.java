/**
 * 这个包是检测工具包.
 *
 * <p>{@link tech.mhuang.pacebox.core.check.CheckAssert} 检测类</p>
 *
 * @author mhuang
 * @since 1.0.0
 */
package tech.mhuang.pacebox.core.check;

