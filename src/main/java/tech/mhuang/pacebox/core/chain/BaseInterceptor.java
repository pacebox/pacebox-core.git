package tech.mhuang.pacebox.core.chain;

/**
 * 基础拦截接口类
 *
 * @author mhuang
 * @since 1.0.2
 */
public interface BaseInterceptor<Chain, Response> {

    /**
     * 拦截
     *
     * @param chain 调用链
     * @return 响应结果
     */
    Response interceptor(Chain chain);
}
