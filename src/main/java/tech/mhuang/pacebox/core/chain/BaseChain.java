package tech.mhuang.pacebox.core.chain;

import java.util.List;

/**
 * 责任链模式
 *
 * @author mhuang
 * @since 1.0.9
 */
public interface BaseChain<Request, Response> {

    /**
     * 添加责任
     * @param interceptor 责任
     * @since 1.1.6
     */
    void add(BaseInterceptor<BaseChain<Request, Response>, Response> interceptor);
    /**
     * 添加责任
     * @param interceptor 责任
     * @param index 责任执行顺序
     * @since 1.1.6
     */
    void add(BaseInterceptor<BaseChain<Request, Response>, Response> interceptor,int index);

    /**
     * 重置责任绑定
     * @param interceptors 绑定
     * @since 1.1.6
     */
    void reset(List<BaseInterceptor<BaseChain<Request, Response>, Response>> interceptors);
    /**
     * 重置责任
     * @since 1.1.6
     */
    void reset();

    /**
     * 获取请求
     *
     * @return 请求
     */
    Request request();

    /**
     * 责任链处理
     *
     * @param request 请求
     * @return 应答数据
     */
    Response proceed(Request request);
}
