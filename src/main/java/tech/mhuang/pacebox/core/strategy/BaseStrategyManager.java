package tech.mhuang.pacebox.core.strategy;

import java.util.Map;

/**
 * 扩展接口
 *
 * @author mhuang
 * @since 1.0.12
 */
public interface BaseStrategyManager<REQUEST, RESPONSE> {

    /**
     * 设置单个event策略
     *
     * @param event          event
     * @param strategyServer 策略服务
     */
    void set(String event, BaseStrategyServer<REQUEST, RESPONSE> strategyServer);

    /**
     * 根据event获取单个策略服务
     *
     * @param event 事件
     * @return 策略服务
     */
    BaseStrategyServer<REQUEST, RESPONSE> get(String event);

    /**
     * 获取所有策略服务
     *
     * @return 策略服务
     */
    Map<String, BaseStrategyServer<REQUEST, RESPONSE>> serverMap();

    /**
     * 刷新策略服务
     *
     * @param serverMap 策略服务
     */
    void refresh(Map<String, BaseStrategyServer<REQUEST, RESPONSE>> serverMap);

    /**
     * 策略执行
     *
     * @param param 策略参数
     * @return 执行结果
     */
    StrategyHandlerResult<RESPONSE> strategy(StrategyHandlerParam<REQUEST> param);


}
