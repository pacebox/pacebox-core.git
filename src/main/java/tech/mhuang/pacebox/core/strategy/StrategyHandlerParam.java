package tech.mhuang.pacebox.core.strategy;

/**
 * 策略请求的数据
 *
 * @author mhuang
 * @since 1.0.12
 */
public class StrategyHandlerParam<T> {

    /**
     * 请求的事件
     */
    private String event;

    /**
     * 请求的数据
     */
    private T data;

    public StrategyHandlerParam() {
    }

    public StrategyHandlerParam(String event, T data) {
        this.event = event;
        this.data = data;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
