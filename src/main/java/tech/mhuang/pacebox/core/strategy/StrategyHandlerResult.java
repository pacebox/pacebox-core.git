package tech.mhuang.pacebox.core.strategy;

/**
 * 策略应答的数据
 *
 * @author mhuang
 * @since 1.0.12
 */
public class StrategyHandlerResult<T> {

    /**
     * 应答状态
     */
    private boolean status;

    /**
     * 应答数据
     */
    private T data;

    /**
     * 异常
     */
    private Throwable throwable;

    public StrategyHandlerResult() {
    }

    public StrategyHandlerResult(boolean status, T data) {
        this.status = status;
        this.data = data;
    }

    public StrategyHandlerResult(boolean status, Throwable throwable) {
        this.status = status;
        this.throwable = throwable;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Throwable getThrowable() {
        return throwable;
    }

    public void setThrowable(Throwable throwable) {
        this.throwable = throwable;
    }
}
