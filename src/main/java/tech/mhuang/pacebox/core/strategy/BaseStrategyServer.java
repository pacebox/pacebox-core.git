package tech.mhuang.pacebox.core.strategy;

import java.util.List;

/**
 * 基础策略抽象接口
 *
 * @author mhuang
 * @since 1.0.12
 */
public interface BaseStrategyServer<REQUEST, RESPONSE> {

    /**
     * 策略执行
     *
     * @param request 请求参数
     * @return 返回执行结果
     */
    StrategyHandlerResult<RESPONSE> strategy(StrategyHandlerParam<REQUEST> request);

    /**
     * 获取当前的event
     *
     * @return event
     */
    List<String> event();


}
