package tech.mhuang.pacebox.core.dict;

import tech.mhuang.pacebox.core.check.CheckAssert;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 字典类
 *
 * @author mhuang
 * @since 1.0.0
 */
public class BasicDict extends LinkedHashMap<String, Object> {

    public BasicDict() {
        super();
    }

    public BasicDict(int initialCapacity) {
        super(initialCapacity);
    }

    public BasicDict(int initialCapacity, float loadFactor) {
        super(initialCapacity, loadFactor);
    }

    public BasicDict(Map<String, Object> m) {
        super(m);
    }

    /**
     * set集合后可继续进行操作
     *
     * @param key   键
     * @param value 值
     * @return BasicDict
     */
    public BasicDict set(String key, Object value) {
        super.put(key, value);
        return this;
    }

    /**
     * 根据键获取对应字典的值.
     *
     * @param key 键
     * @param <T> 转换后值的类型
     * @return 转换后的对象
     */
    public <T> T get(String key) {
        return get(key, null);
    }

    /**
     * 根据健获取对应字典的值
     *
     * @param key          键
     * @param defaultValue 默认值
     * @param <T>          转换后值的类型
     * @return 转换后的对象
     */
    public <T> T get(String key, Object defaultValue) {
        return (T) super.getOrDefault(key, defaultValue);
    }

    /**
     * 转换
     * @param key 对象
     * @param defaultClass 默认类型
     * @return 结果
     * @param <T> 对象得类型
     * @since 1.1.7
     */
    public <T> T get(String key,Class<T> defaultClass){
        CheckAssert.check(defaultClass, "not class");
        return (T) super.get(key);
    }
}
