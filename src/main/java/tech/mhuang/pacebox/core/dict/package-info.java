/**
 * 字典包.
 * <p>字典使用工具包</p>
 * <ul>
 *     <li>{@link tech.mhuang.pacebox.core.dict.BasicDict} 字典工具类</li>
 * </ul>
 *
 * @author mhuang
 * @since 1.0.0
 */
package tech.mhuang.pacebox.core.dict;