package tech.mhuang.pacebox.core.customize;


/**
 * 定制模型
 *
 * @author mhuang
 * @since 1.0.0
 */
@FunctionalInterface
public interface Customizer<T> {

    /**
     * 定制
     *
     * @param t 具体类
     */
    void customize(T t);
}
