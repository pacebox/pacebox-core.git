package tech.mhuang.pacebox.core.codec;

/**
 * 解码
 *
 * @author mhuang
 * @since 1.1.6
 */
public interface Decoder<T, R> {

    /**
     * 解码
     *
     * @param data 需要解码的数据
     * @return 解码结果
     */
    R decode(T data);
}
