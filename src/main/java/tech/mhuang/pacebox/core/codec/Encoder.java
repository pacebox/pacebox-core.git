package tech.mhuang.pacebox.core.codec;

/**
 * 编码
 *
 * @author mhuang
 * @since 1.1.6
 */
public interface Encoder<T, R> {

    /**
     * 编码
     *
     * @param data 编码数据
     * @return 解码数据
     */
    R encode(T data);

}
