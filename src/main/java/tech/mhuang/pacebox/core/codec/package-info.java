/**
 * 加密解密.
 *
 * <p>加密解密</p>
 * <ul>
 * <li>{@link tech.mhuang.pacebox.core.codec.Base16} Base16</li>
 * <li>{@link tech.mhuang.pacebox.core.codec.Base32} Base32</li>
 * </ul>
 *
 * @author mhuang
 * @since 1.1.6
 */
package tech.mhuang.pacebox.core.codec;