package tech.mhuang.pacebox.core.compress;

import java.io.IOException;
import java.util.Map;

/**
 * 压缩工具类整合接口
 *
 * @author yuanhang.huang
 * @since 1.0.9
 */
public interface BaseCompress<T extends BaseCompressHandler> {

    /**
     * 获取handler集合
     *
     * @return Handler的Map集合
     */
    Map<String, T> getHandlerMap();

    /**
     * 添加handler
     *
     * @param key     handler对应的key值
     * @param handler BaseCompressHandler对象
     */
    void addHandler(String key, T handler);

    /**
     * 删除handler
     *
     * @param key handler对应的key值
     */
    void delHandler(String key);

    /**
     * 获取默认的CompressHandler，默认为ZipCompressHandler
     *
     * @return BaseCompressHandler对象
     */
    T getDefaultHandler();

    /**
     * 设置默认的CompressHandler
     *
     * @param compress BaseCompressHandler对象
     */
    void setDefaultHandler(T compress);

    /**
     * 根据压缩文件的名字获取CompressHandler
     *
     * @param fileName 文件名字
     * @return BaseCompressHandler对象
     */
    T getAutoHandler(String fileName);

    /**
     * 根据Key值获取CompressHandler
     *
     * @param key CompressHandler的标识
     * @return BaseCompressHandler对象
     */
    T getHandler(String key);

    /**
     * 压缩文件，根据设置的默认CompressHandler压缩，默认为ZipCompressHandler
     *
     * @param sourcePath 源文件/文件夹路径
     * @param destFile   压缩文件
     * @param cover      是否覆盖
     * @throws IOException          IOException
     * @throws InterruptedException InterruptedException
     */
    void compress(String sourcePath, String destFile, boolean cover) throws IOException, InterruptedException;

    /**
     * 压缩文件
     *
     * @param sourcePath 源文件/文件夹路径
     * @param destFile   压缩文件
     * @param cover      是否覆盖
     * @param key        handler对应的key值
     * @throws IOException          IOException
     * @throws InterruptedException InterruptedException
     */
    void compress(String sourcePath, String destFile, boolean cover, String key) throws IOException, InterruptedException;

    /**
     * 解压文件，根据后辍自动识别
     *
     * @param sourceFile 源压缩文件路径
     * @param destDir    解压到文件夹
     * @param cover      是否覆盖
     * @param deep       深度解压（递归）
     * @throws IOException          IOException
     * @throws InterruptedException InterruptedException
     */
    void decompress(String sourceFile, String destDir, boolean cover, boolean deep) throws IOException, InterruptedException;

    /**
     * 解压文件
     *
     * @param sourceFile 源压缩文件路径
     * @param destDir    解压到文件夹
     * @param cover      是否覆盖
     * @param deep       深度解压（递归）
     * @param key        handler对应的key值
     * @throws IOException          IOException
     * @throws InterruptedException InterruptedException
     */
    void decompress(String sourceFile, String destDir, boolean cover, boolean deep, String key) throws IOException, InterruptedException;

}
