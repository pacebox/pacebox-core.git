package tech.mhuang.pacebox.core.compress;

import java.io.IOException;

/**
 * 压缩工具基础接口
 *
 * @author yuanhang.huang
 * @since 1.0.9
 */
public interface BaseCompressHandler {

    /**
     * 压缩文件匹配检查
     *
     * @param fileName 压缩文件名
     * @return 是否匹配成功
     */
    boolean match(String fileName);

    /**
     * 压缩文件
     *
     * @param sourcePath 源文件/文件夹
     * @param destFile   压缩文件
     * @param cover      覆盖文件
     * @throws IOException          IO异常
     * @throws InterruptedException 中断异常
     * @throws RuntimeException     运行异常
     */
    void compress(String sourcePath, String destFile, boolean cover) throws IOException, InterruptedException, RuntimeException;

    /**
     * 解压文件
     *
     * @param sourceFile 源文件
     * @param destDir    解压目录
     * @param cover      覆盖文件
     * @param deep       深度压缩（递归）
     * @throws IOException          IO异常
     * @throws InterruptedException 中断异常
     * @throws RuntimeException     运行异常
     */
    void decompress(String sourceFile, String destDir, boolean cover, boolean deep) throws IOException, InterruptedException, RuntimeException;


}
