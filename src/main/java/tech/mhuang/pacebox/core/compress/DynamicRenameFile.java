package tech.mhuang.pacebox.core.compress;


import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * 动态重命名文件
 *
 * @author mhuang
 * @since 1.1.6
 */
public class DynamicRenameFile {

    /**
     * 重命名的文件夹
     */
    private String dirName;

    /**
     * 重命名的文件列表
     */
    private List<RenameFile> fileList;

    public String getDirName() {
        return dirName;
    }

    public void setDirName(String dirName) {
        this.dirName = dirName;
    }

    public List<RenameFile> getFileList() {
        return fileList;
    }

    public void setFileList(List<RenameFile> fileList) {
        this.fileList = fileList;
    }

    public DynamicRenameFile(){
        fileList = new ArrayList<>();
    }

    public DynamicRenameFile(Integer size){
       fileList = new ArrayList<>(size);
    }
    public static RenameFile renameFile(String rename,File file){
        RenameFile renameFile = new RenameFile();
        renameFile.setRename(rename);
        renameFile.setFile(file);
        return renameFile;
    }

    public void addRenameFile(String rename,File file) {
        this.fileList.add(renameFile(rename, file));
    }
}
