package tech.mhuang.pacebox.core.compress;


import java.io.File;

/**
 * 重命名文件
 *
 * @author mhuang
 * @since 1.1.6
 */
public class RenameFile{

    /**
     * 重命名的文件名
     */
    private String rename;

    /**
     * 文件
     */
    private File file;

    public String getRename() {
        return rename;
    }

    public void setRename(String rename) {
        this.rename = rename;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }
}