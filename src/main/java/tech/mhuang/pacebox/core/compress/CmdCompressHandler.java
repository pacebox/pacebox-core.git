package tech.mhuang.pacebox.core.compress;

/**
 * 命令压缩解压基础接口
 *
 * @author yuanhang.huang
 * @since 1.0.9
 */
public interface CmdCompressHandler extends BaseCompressHandler {

    /**
     * 获取命令程序所在文件夹路径
     *
     * @return 路径
     */
    String getCmdDir();

    /**
     * 设置命令程序所在文件夹路径
     *
     * @param cmdDir 路径
     */
    void setCmdDir(String cmdDir);

}
