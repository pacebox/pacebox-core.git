package tech.mhuang.pacebox.core.compress;

import java.io.File;
import java.util.List;

/**
 * 动态文件
 *
 * @author mhuang
 * @since 1.1.6
 */
public class DynamicFile {

    /**
     * 重命名的文件夹
     */
    private String dirName;

    /**
     * 重命名的文件列表
     */
    private List<File> fileList;

    public String getDirName() {
        return dirName;
    }

    public void setDirName(String dirName) {
        this.dirName = dirName;
    }

    public List<File> getFileList() {
        return fileList;
    }

    public void setFileList(List<File> fileList) {
        this.fileList = fileList;
    }
}
