package tech.mhuang.pacebox.core.compress;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

/**
 * 代码压缩解压的基础接口
 *
 * @author yuanhang.huang
 * @since 1.0.9
 */
public interface CodeCompressHandler extends BaseCompressHandler {

    /**
     * 源文件/文件夹压缩成流数据输出
     *
     * @param sourcePath 源文件/文件夹
     * @param os         输出流
     * @throws RuntimeException RuntimeException
     * @throws IOException      IOException
     */
    void compress(File sourcePath, OutputStream os) throws RuntimeException, IOException;

}
