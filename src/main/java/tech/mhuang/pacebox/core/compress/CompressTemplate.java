package tech.mhuang.pacebox.core.compress;

import tech.mhuang.pacebox.core.compress.handler.RarCmdCompressHandler;
import tech.mhuang.pacebox.core.compress.handler.ZipCodeCompressHandler;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 压缩工具类整合实现
 *
 * @author yuanhang.huang
 * @since 1.0.9
 */
public class CompressTemplate implements BaseCompress {

    /**
     * CompressHandler 集合
     */
    private static Map<String, BaseCompressHandler> handlerMap = new ConcurrentHashMap<>();

    /**
     * 单例 CompressTemplate
     */
    private static CompressTemplate compressTemplate = new CompressTemplate();

    /**
     * 默认的 CompressHandler
     */
    private BaseCompressHandler compressHandler;

    private CompressTemplate() {

        handlerMap.put(ZipCodeCompressHandler.CODE, new ZipCodeCompressHandler());
        handlerMap.put(RarCmdCompressHandler.CODE, new RarCmdCompressHandler());

        this.compressHandler = handlerMap.get(ZipCodeCompressHandler.CODE);

    }

    public static BaseCompress getInstance() {
        return compressTemplate;
    }

    @Override
    public Map<String, BaseCompressHandler> getHandlerMap() {
        return handlerMap;
    }

    @Override
    public void addHandler(String key, BaseCompressHandler handler) {
        handlerMap.put(key, handler);
    }

    @Override
    public void delHandler(String key) {
        handlerMap.remove(key);
    }

    @Override
    public BaseCompressHandler getDefaultHandler() {
        return handlerMap.get(ZipCodeCompressHandler.CODE);
    }

    @Override
    public void setDefaultHandler(BaseCompressHandler compress) {
        this.compressHandler = compress;
    }

    @Override
    public BaseCompressHandler getAutoHandler(String fileName) {
        for (BaseCompressHandler handler : handlerMap.values()) {
            if (handler.match(fileName)) {
                return handler;
            }
        }
        return null;
    }

    @Override
    public BaseCompressHandler getHandler(String key) {
        return handlerMap.get(key);
    }

    @Override
    public void compress(String sourcePath, String destFile, boolean cover) throws IOException, InterruptedException {
        getDefaultHandler().compress(sourcePath, destFile, cover);
    }

    @Override
    public void compress(String sourcePath, String destFile, boolean cover, String key) throws IOException, InterruptedException {
        handlerMap.get(key).compress(sourcePath, destFile, cover);
    }

    @Override
    public void decompress(String sourceFile, String destDir, boolean cover, boolean deep) throws IOException, InterruptedException {
        getAutoHandler(sourceFile).decompress(sourceFile, destDir, cover, deep);
    }

    @Override
    public void decompress(String sourceFile, String destDir, boolean cover, boolean deep, String key) throws IOException, InterruptedException {
        handlerMap.get(key).decompress(sourceFile, destDir, cover, deep);
    }

}
