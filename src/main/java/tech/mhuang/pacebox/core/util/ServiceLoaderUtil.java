package tech.mhuang.pacebox.core.util;

import java.util.ServiceLoader;

/**
 * ServiceLoader工具类(SPI扩展)
 *
 * @author mhuang
 * @since 1.1.6
 */
public class ServiceLoaderUtil {

    /**
     * 加载服务
     *
     * @param <T>   接口类型
     * @param clazz 服务接口
     * @return 服务接口实现列表
     */
    public static <T> ServiceLoader<T> load(Class<T> clazz) {
        return load(clazz, null);
    }

    /**
     * 加载服务
     *
     * @param <T>    接口类型
     * @param clazz  服务接口
     * @param loader {@link ClassLoader}
     * @return 服务接口实现列表
     */
    public static <T> ServiceLoader<T> load(Class<T> clazz, ClassLoader loader) {
        return ServiceLoader.load(clazz, ObjectUtil.ifNullDefault(loader, ClassLoaderUtil::getClassLoader));
    }
}
