package tech.mhuang.pacebox.core.util;

import java.util.concurrent.TimeUnit;

/**
 * 线程工具类
 *
 * @author mhuang
 * @since 1.1.7
 */
public class ThreadUtil {

    /**
     * 休眠时间
     *
     * @param millis 毫秒
     */
    public static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
        }
    }

    /**
     * 休眠时间
     *
     * @param duration 时间数
     * @param unit     时间类型
     */
    public static void sleep(long duration, TimeUnit unit) {
        try {
            Thread.sleep(unit.toMillis(duration));
        } catch (InterruptedException e) {
        }
    }
}
