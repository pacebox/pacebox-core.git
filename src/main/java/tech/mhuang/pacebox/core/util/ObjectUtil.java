package tech.mhuang.pacebox.core.util;

import java.lang.reflect.Array;
import java.util.function.Supplier;

/**
 * Object工具类
 *
 * @author mhuang
 * @since 1.0.0
 */
public class ObjectUtil {

    /**
     * 判断对象是否为空
     *
     * @param object 对象
     * @return 若为空则返回true、不为空返回false
     */
    public static boolean isEmpty(Object object) {
        return object == null;
    }

    /**
     * 判断2个对象是否相同
     *
     * @param current 当前对象
     * @param compare 比较对象
     * @return True相等 False不相等
     * @since 1.0.6
     */
    public static Boolean equals(Object current, Object compare) {
        if (current == compare) {
            return true;
        }
        if (current == null) {
            return false;
        }
        return current.equals(compare);
    }

    /**
     * 判断对象是否不为空
     *
     * @param object 对象
     * @return 若为空则返回false、不为空返回true
     */
    public static boolean isNotEmpty(Object object) {
        return !isEmpty(object);
    }

    /**
     * 数组下标添加一个元素
     *
     * @param array 数组
     * @param obj   元素
     * @param <A>   数组的类型
     * @param <O>   元素的类型
     * @return 添加后的数组
     * @since 1.1.4
     */
    public static <A, O extends A> A[] addObjectToArray(A[] array, O obj) {
        Class<?> compType = Object.class;
        if (array != null) {
            compType = array.getClass().getComponentType();
        } else if (obj != null) {
            compType = obj.getClass();
        }
        int newArrLength = (array != null ? array.length + 1 : 1);
        A[] newArr = (A[]) Array.newInstance(compType, newArrLength);
        if (array != null) {
            System.arraycopy(array, 0, newArr, 0, array.length);
        }
        newArr[newArr.length - 1] = obj;
        return newArr;
    }

    /**
     * 判断对象是否是数组
     *
     * @param obj 对应
     * @return 若是数组则为true，否则是false
     * @since 1.1.4
     */
    public static boolean isArray(Object obj) {
        return (obj != null && obj.getClass().isArray());
    }

    /**
     * 判断是否不相等
     *
     * @param current 当前对象
     * @param compare 比较对象
     * @return 是否不相等
     */
    public static Boolean notEquals(Object current, Object compare) {
        return !equals(current, compare);
    }

    /**
     * 如果数据为空则取根据函数获取
     * @param data 数据
     * @param defaultSupplier 默认值
     * @return 结果
     * @param <T> 类型
     * @since 1.1.6
     */
    public static <T> T ifNullDefault(T data, Supplier<? extends T> defaultSupplier) {
        if(isEmpty(data)){
            return defaultSupplier.get();
        }
        return data;
    }
    /**
     * 如果数据为空则取默认函数、否则根据函数获取
     * @param data 数据
     * @param handle 函数
     * @param defualtSupplier 默认函数
     * @return 结果
     * @param <T> 类型
     * @since 1.1.6
     */
    public static <T> T ifNullDefault(String data, Supplier<? extends T> handle, Supplier<? extends T> defualtSupplier) {
        if (ObjectUtil.isNotEmpty(data)) {
            return handle.get();
        }
        return defualtSupplier.get();
    }

    /**
     * 如果数据为空则取默认值、否则根据函数获取
     * @param data 数据
     * @param handle 函数
     * @param defaultValue 默认值
     * @return 结果
     * @param <T> 类型
     * @since 1.1.6
     */
    public static <T> T ifNullDefault(String data, Supplier<? extends T> handle, final T defaultValue) {
        if (ObjectUtil.isNotEmpty(data)) {
            return handle.get();
        }
        return defaultValue;
    }
    /**
     * 如果数据为空则取默认值
     * @param data 数据
     * @param defaultData 默认值
     * @return 结果
     * @param <T> 类型
     * @since 1.1.6
     */
    public static <T> T ifNullDefault(T data,final T defaultData) {
        if(isEmpty(data)){
            return defaultData;
        }
        return data;
    }
}
