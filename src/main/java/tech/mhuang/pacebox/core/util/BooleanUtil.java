package tech.mhuang.pacebox.core.util;

/**
 * boolean工具类
 *
 * @author mhuang
 * @since 1.1.2
 */
public class BooleanUtil {


    /**
     * boolean转int
     *
     * @param value boolean
     * @return int
     */
    public static int toInt(boolean value) {
        return value ? 1 : 0;
    }

    /**
     * boolean转integer
     *
     * @param value boolean
     * @return integer
     */
    public static Integer toInteger(boolean value) {
        return toInt(value);
    }

    /**
     * boolean值转为char
     *
     * @param value boolean
     * @return char
     */
    public static char toChar(boolean value) {
        return (char) toInt(value);
    }

    /**
     * boolean值转为character
     *
     * @param value boolean
     * @return character
     */
    public static Character toCharacter(boolean value) {
        return toChar(value);
    }

    /**
     * boolean值转为byte
     *
     * @param value boolean
     * @return byte
     */
    public static byte toByte(boolean value) {
        return (byte) toInt(value);
    }

    /**
     * boolean值转为Byte
     *
     * @param value boolean
     * @return Byte
     */
    public static byte toByteObj(boolean value) {
        return toByte(value);
    }
}
