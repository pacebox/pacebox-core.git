package tech.mhuang.pacebox.core.util;


/**
 * 16进制工具类
 *
 * @author mhuang
 * @since 1.1.6
 */
public class HexUtil {

    /**
     * 转为16进制字符串
     * @param value long值
     * @return 16进制
     */
    public static String toHex(long value) {
        return Long.toHexString(value);
    }

    /**
     * 16进制字符串转为long
     * @param value 16进制字符串
     * @return long值
     */
    public static long hexToLong(String value) {
        return Long.parseLong(value, 16);
    }
}
