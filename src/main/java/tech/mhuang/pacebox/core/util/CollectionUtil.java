package tech.mhuang.pacebox.core.util;


import tech.mhuang.pacebox.core.exception.BusinessException;

import java.lang.reflect.Constructor;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 集合工具类
 *
 * @author mhuang
 * @since 1.0.0
 */
public class CollectionUtil {

    private CollectionUtil() {

    }

    /**
     * 扩容获取对象
     *
     * @param clazz    对象
     * @param capacity 扩容值
     * @param <T>      对象的类型
     * @return 对象
     * @since 1.0.16
     */
    public static <T> T capacity(Class<T> clazz, int capacity) {
        try {
            Constructor<T> constructor = clazz.getConstructor(int.class);
            if (Map.class.isAssignableFrom(clazz) || Set.class.isAssignableFrom(clazz)) {
                //map or set default capacity very
                capacity = Math.max((int) (capacity / .75f) + 1, 16);
            }
            return constructor.newInstance(capacity);
        } catch (Exception e) {
            throw new BusinessException(e);
        }
    }


    /**
     * 获取子集map对象
     *
     * @param map  map
     * @param keys 子级可能的keys
     * @param <K>  map的key类型
     * @param <V>  map的value类型
     * @return 结果集
     * @since 1.1.7
     */
    public static <K, V> Map<K, V> getSubKeys(Map<K, V> map, K... keys) {
        return getSubKeys(map, Stream.of(keys).collect(Collectors.toSet()));
    }

    /**
     * 获取子集map对象
     *
     * @param map  map
     * @param keys 子级可能的keys
     * @param <K>  map的key类型
     * @param <V>  map的value类型
     * @return 结果集
     * @since 1.1.7
     */
    public static <K, V> Map<K, V> getSubKeys(Map<K, V> map, Iterable<K> keys) {
        Map<K, V> resultMap = capacity(LinkedHashMap.class, 20);
        for (K key : keys) {
            if (map.containsKey(key)) {
                resultMap.put(key, map.get(key));
            }
        }
        return resultMap;
    }


    /**
     * 获取子集map对象不包含key
     *
     * @param map  map
     * @param keys 子级可能的keys
     * @param <K>  map的key类型
     * @param <V>  map的value类型
     * @return 结果集
     * @since 1.1.7
     */
    public static <K, V> Map<K, V> getSubExcludeKeys(Map<K, V> map, K... keys) {
        return getSubExcludeKeys(map, Stream.of(keys).collect(Collectors.toSet()));
    }

    /**
     * 获取子集map对象不包含key
     *
     * @param map  map
     * @param keys 子级可能的keys
     * @param <K>  map的key类型
     * @param <V>  map的value类型
     * @return 结果集
     * @since 1.1.7
     */
    public static <K, V> Map<K, V> getSubExcludeKeys(Map<K, V> map, Iterable<K> keys) {
        Map<K, V> resultMap = capacity(LinkedHashMap.class, 20);
        for (K key : keys) {
            if (!map.containsKey(key)) {
                resultMap.put(key, map.get(key));
            }
        }
        return resultMap;
    }

    /**
     * 删除map中的key
     *
     * @param map  map
     * @param keys key数组
     * @param <K>  key类型
     * @param <V>  value类型
     * @since 1.1.7
     */
    public static <K, V> void removeKeys(Map<K, V> map, K... keys) {
        removeKeys(map, Stream.of(keys).collect(Collectors.toSet()));
    }

    /**
     * 删除map中的key
     *
     * @param map  map
     * @param keys key集合
     * @param <K>  key类型
     * @param <V>  value类型
     * @since 1.1.7
     */
    public static <K, V> void removeKeys(Map<K, V> map, Iterable<K> keys) {
        for (K key : keys) {
            if (map.containsKey(key)) {
                map.remove(key);
            }
        }
    }

    /**
     * 根据删除的数据进行元素删除，返回新集合
     * @param collect 总元素
     * @param removeCollect 删除的元素
     * @return 结果集
     * @param <V> 元素类型
     * @since 1.1.7
     */
    public static <V> Collection<V> removeAll(Collection<V> collect,V... removeCollect){
        return removeAll(collect,Stream.of(removeCollect).collect(Collectors.toSet()));
    }

    /**
     * 根据删除的数据进行元素删除，返回新集合
     * @param collect 总元素
     * @param removeCollect 删除的元素
     * @return 结果集
     * @param <V> 元素类型
     * @since 1.1.7
     */
    public static <V> Collection<V> removeAll(Collection<V> collect,Collection<V> removeCollect){
        Set<V> set = new HashSet<>(collect);
        Set<V> removeSet = new HashSet<>(removeCollect);
        Collection<V> returnSet = new HashSet<>(removeSet.size());
        for (V data : set){
            if(!removeSet.contains(data)){
                returnSet.add(data);
            }
        }
        return returnSet;
    }
    /**
     * 根据查询的数据进行元素查询，返回新集合
     * @param collect 总元素
     * @param removeCollect 查询的元素
     * @return 结果集
     * @param <V> 元素类型
     * @since 1.1.7
     */
    public static <V> Collection<V> searchAll(Collection<V> collect,V... removeCollect){
        return searchAll(collect,Stream.of(removeCollect).collect(Collectors.toSet()));
    }

    /**
     * 根据查询的数据进行元素查询，返回新集合
     * @param collect 总元素
     * @param removeCollect 查询的元素
     * @return 结果集
     * @param <V> 元素类型
     * @since 1.1.7
     */
    public static <V> Collection<V> searchAll(Collection<V> collect,Collection<V> removeCollect){
        Set<V> set = new HashSet<>(collect);
        Set<V> removeSet = new HashSet<>(removeCollect);
        Collection<V> returnSet = new HashSet<>(removeSet.size());
        for (V data : set){
            if(removeSet.contains(data)){
                returnSet.add(data);
            }
        }
        return returnSet;
    }


    /**
     * 如果提供的Collection为{@code null}或为空，则返回{@code true}。
     * 否则，请返回{@code false}。
     *
     * @param collection 集合
     * @return boolean 给定的Collection是否为空
     */
    public static boolean isEmpty(Collection<?> collection) {
        return (collection == null || collection.isEmpty());
    }

    /**
     * 如果提供的Collection不为{@code null}或不为空，则返回{@code true}。
     * 否则，请返回{@code false}。
     *
     * @param collection 集合
     * @return boolean 给定的Collection是否不为空
     */
    public static boolean isNotEmpty(Collection<?> collection) {
        return !isEmpty(collection);
    }

    /**
     * 如果提供的数组对象为为{@code null}或为空，则返回{@code true}。
     * 否则，请返回{@code false}。
     *
     * @param obj 检查这个数组
     * @param <T> 数组类型
     * @return 给定的数组是否为空
     */
    public static <T> boolean isEmpty(T[] obj) {
        return obj == null || obj.length == 0;
    }

    /**
     * 如果提供的数组不为{@code null}或不为空，则返回{@code true}。
     * 否则，请返回{@code false}。
     *
     * @param <T> 类型
     * @param obj 检查这个obj
     * @return 给定的数组是否不为空
     * @since 1.1.2
     */
    public static <T> boolean isNotEmpty(T[] obj) {
        return !isEmpty(obj);
    }

    /**
     * 空值默认
     *
     * @param array        数组
     * @param defaultArray 默认数组
     * @param <T>          类型
     * @return 给定的数组不为空返回数组，否则返回默认数组
     */
    public static <T> T[] defaultEmpty(T[] array, T[] defaultArray) {
        return isEmpty(array) ? defaultArray : array;
    }

    /**
     * 如果提供的Map为{@code null}或为空，则返回{@code true}。
     * 否则，请返回{@code false}。
     *
     * @param map 检查这个map
     * @return 给定的Map是否为空
     */
    public static boolean isEmpty(Map<?, ?> map) {
        return (map == null || map.isEmpty());
    }

    /**
     * 如果提供的Map不为{@code null}或不为空，则返回{@code true}。
     * 否则，请返回{@code false}。
     *
     * @param map 检查这个map
     * @return 给定的Map是否不为空
     */
    public static boolean isNotEmpty(Map<?, ?> map) {
        return !isEmpty(map);
    }
}
