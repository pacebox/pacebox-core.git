/**
 * 这个包是工具使用包.
 *
 * <p>主要是用于数据转换判断等操作</p>
 * <ul>
 * <li>{@link tech.mhuang.pacebox.core.util.CryptoUtil} 加密工具类</li>
 * <li>{@link tech.mhuang.pacebox.core.util.PathUtil} 路径工具类</li>
 * <li>{@link tech.mhuang.pacebox.core.util.StringUtil} 字符串工具类</li>
 * <li>{@link tech.mhuang.pacebox.core.util.CollectionUtil} 集合工具类</li>
 * <li>{@link tech.mhuang.pacebox.core.util.ObjectUtil} 对象工具类</li>
 * <li>{@link tech.mhuang.pacebox.core.util.CharSequenceUtil} 字符序列工具类</li>
 * <li>{@link tech.mhuang.pacebox.core.util.ZipUtil} zip压缩文件工具类</li>
 * <li>{@link tech.mhuang.pacebox.core.util.RegexUtil} 常用正则工具类</li>
 * <li>{@link tech.mhuang.pacebox.core.util.HexUtil} 16进制工具类</li>
 * <li>{@link tech.mhuang.pacebox.core.util.ClassLoaderUtil} ClassLoader工具类</li>
 * <li>{@link tech.mhuang.pacebox.core.util.ServiceLoaderUtil} ServiceLoader工具类</li>
 * </ul>
 *
 * @author mhuang
 * @since 1.0.0
 */
package tech.mhuang.pacebox.core.util;