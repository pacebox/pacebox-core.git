/**
 * 这个包是物理分页处理使用包.
 *
 * <p>主要用于物理分页相关操作</p>
 * <ul>
 * <li>{@link tech.mhuang.pacebox.core.page.PageParam} 物理分页参数</li>
 * <li>{@link tech.mhuang.pacebox.core.page.PageUtil} 物理分页工具类</li>
 * </ul>
 *
 * @author mhuang
 * @since 1.1.2
 */
package tech.mhuang.pacebox.core.page;