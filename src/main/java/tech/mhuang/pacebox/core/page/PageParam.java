package tech.mhuang.pacebox.core.page;

import java.util.Objects;

/**
 * 分页条件
 *
 * @author mhuang
 * @since 1.1.2
 */
public class PageParam {

    /**
     * 最大值
     */
    private int max;

    /**
     * 类型 1代表根据数量，2代表切分多少分
     */
    private int type;

    /**
     * 类型的值
     */
    private int num;

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PageParam that = (PageParam) o;
        return max == that.max;
    }

    /**
     * 初始化一页最大数据
     *
     * @param max 最大值
     * @return 分页参数
     */
    public static PageParam init(Integer max) {
        PageParam pageParam = new PageParam();
        pageParam.setMax(max);
        pageParam.setType(1);
        pageParam.setNum(max);
        return pageParam;
    }

    public PageParam() {

    }

    public PageParam(Integer max) {
        this.setMax(max);
        this.setType(1);
        this.setNum(max);
    }

    public PageParam(Integer max, Integer num) {
        this.setMax(max);
        this.setType(1);
        this.setNum(num);
    }

    public PageParam(Integer max, Integer type, Integer num) {
        this.setMax(max);
        this.setType(type);
        this.setNum(num);
    }

    @Override
    public int hashCode() {
        return Objects.hash(max);
    }
}
