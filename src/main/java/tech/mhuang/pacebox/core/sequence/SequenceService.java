package tech.mhuang.pacebox.core.sequence;

/**
 * 序列号接口服务
 *
 * @author mhuang
 * @since 1.1.7
 */
public interface SequenceService {

    /**
     * 获取序号
     *
     * @param name 序号名称
     * @return 序号值
     */
    long next(String name);

    /**
     * 获取序号
     *
     * @param name 序号名称
     * @return 序号值
     */
    String nextValue(String name);
}
