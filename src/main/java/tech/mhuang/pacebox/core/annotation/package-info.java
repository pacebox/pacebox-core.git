/**
 * 注解工具包.
 * <p>注解工具包</p>
 * <ul>
 * <li>{@link tech.mhuang.pacebox.core.annotation.Idempotent} 幂等</li>
 * </ul>
 *
 * @author mhuang
 * @since 1.1.7
 */
package tech.mhuang.pacebox.core.annotation;