package tech.mhuang.pacebox.core.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 幂等注解
 *
 * @author mhuang
 * @since 1.1.7
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Idempotent {

    boolean enable() default true;

    /**
     * 名称, 用来区分需要不同控制的方法
     * @return
     */
    String name() default "";

    /**
     * 超时时间、默认5秒
     * @return
     */
    long timeout() default 5 * 1000;

    /**
     * 提示信息
     * @return
     */
    String message() default "重复请求";
}
