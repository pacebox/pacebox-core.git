/**
 * 网络工具包.
 * <p>用于封装工具相关调用</p>
 * <ul>
 *  <li>{@link tech.mhuang.pacebox.core.network.DefaultX509TrustManager} X509证管理</li>
 *  <li>{@link tech.mhuang.pacebox.core.network.URLAuthenticator} 授权管理</li>
 * </ul>
 *
 * @author mhuang
 * @since 1.1.7
 */
package tech.mhuang.pacebox.core.network;