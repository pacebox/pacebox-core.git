package tech.mhuang.pacebox.core.network;

import java.net.Authenticator;
import java.net.PasswordAuthentication;

/**
 * URL授权
 *
 * @author mhuang
 * @since 1.1.7
 */
public class URLAuthenticator extends Authenticator {

    private String username;
    private String password;

    public URLAuthenticator(String username, String password) {
        this.username = username;
        this.password = password;
    }

    @Override
    protected PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication(username, password.toCharArray());
    }
}
