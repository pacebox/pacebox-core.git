package tech.mhuang.pacebox.core.listen;

/**
 * 监听上下文
 *
 * @author mhuang
 * @since 1.0.2
 */
public interface BaseListener<Request, Response, Context> {

    /**
     * 执行前监听上下文
     *
     * @param request 请求
     * @param context 上下文
     */
    void onRequest(Request request, Context context);

    /**
     * 请求后监听上下文
     *
     * @param request  请求
     * @param response 应答
     * @param context  上下文
     */
    void onResponse(Request request, Response response, Context context);

    /**
     * 请求异常监听上下文
     *
     * @param request 请求
     * @param e       异常
     * @param context 上下文
     */
    void onError(Request request, Throwable e, Context context);
}
