/**
 * 关闭包
 *
 * <p>{@link tech.mhuang.pacebox.core.close.BaseCloseable} 关闭接口</p>
 *
 * @author mhuang
 * @since 1.0.0
 */
package tech.mhuang.pacebox.core.close;