package tech.mhuang.pacebox.core.hit;

import java.util.Objects;

/**
 * 命中实体
 *
 * @author mhuang
 * @since 1.1.2
 */
public class Hit<T> {

    /**
     * 参数
     */
    private T value;

    /**
     * 出现次数
     */
    private Integer count;

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o){
            return true;
        }
        if (o == null || getClass() != o.getClass()){
            return false;
        }
        Hit hit = (Hit) o;
        return value.equals(hit.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
