/**
 * 这个包是命中处理使用包.
 *
 * <p>主要是用于数据获取最佳达到目标的方式</p>
 * <ul>
 * <li>{@link tech.mhuang.pacebox.core.hit.Hit} 命中类</li>
 * <li>{@link tech.mhuang.pacebox.core.hit.HitUtil} 命中工具处理类</li>
 * </ul>
 *
 * @author mhuang
 * @since 1.1.2
 */
package tech.mhuang.pacebox.core.hit;