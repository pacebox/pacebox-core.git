package tech.mhuang.pacebox.core.observer;

import java.util.concurrent.ExecutionException;

/**
 * 观察者接口
 *
 * @author mhuang
 * @since 1.0.0
 */
public interface BaseObServer<T> {

    /**
     * 获取观察者的名称
     *
     * @return 观察者的名称
     */
    String getName();

    /**
     * 获取观察者的通知类型.
     *
     * @return 同步or异步
     */
    ObserverType getType();

    /**
     * 观察者执行接口
     *
     * @param data 请求的数据
     * @throws ExecutionException   执行异常
     * @throws InterruptedException 断开异常
     */
    void execute(T data) throws ExecutionException, InterruptedException;
}
