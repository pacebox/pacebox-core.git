/**
 * 语法糖包.
 * <p>用于封装jdk8插件处理类</p>
 * <ul>
 *  <li>{@link tech.mhuang.pacebox.core.plugin.PluginClassLoader} 插件</li>
 * </ul>
 *
 * @author mhuang
 * @since 1.1.7
 */
package tech.mhuang.pacebox.core.plugin;