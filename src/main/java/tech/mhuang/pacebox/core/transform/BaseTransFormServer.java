package tech.mhuang.pacebox.core.transform;

import java.util.List;

/**
 * 通用转换服务
 *
 * @author mhuang
 * @since 1.0.13
 */
public interface BaseTransFormServer<Source, Target> {

    /**
     * 原转换到目标
     *
     * @param source 原对象
     * @return 目标对象
     */
    Target toTarget(Source source);

    /**
     * 原转换到目标
     *
     * @param source 原对象
     * @return 目标对象
     */
    List<Target> toTarget(List<Source> source);

    /**
     * 目标转换到原
     *
     * @param target 目标对象
     * @return 原对象
     */
    Source toSource(Target target);

    /**
     * 目标转换到原
     *
     * @param target 目标对象
     * @return 原对象
     */
    List<Source> toSource(List<Target> target);
}
