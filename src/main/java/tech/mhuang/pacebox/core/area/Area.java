package tech.mhuang.pacebox.core.area;

import java.util.Objects;

/**
 * 区域
 *
 * @author mhuang
 * @since 1.1.2
 */
public class Area<T> {

    /**
     * 区域code
     */
    private String code;

    /**
     * 区域名称
     */
    private String name;

    /**
     * 区域上级code
     */
    private String parentCode;

    /**
     * 区域其他数据
     */
    private T data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParentCode() {
        return parentCode;
    }

    public void setParentCode(String parentCode) {
        this.parentCode = parentCode;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o){
            return true;
        }
        if (o == null || getClass() != o.getClass()){
            return false;
        }
        Area<T> area = (Area<T>) o;
        return code.equals(area.code);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code);
    }
}
