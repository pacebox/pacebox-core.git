/**
 * 这个包是区域处理使用包.
 *
 * <p>主要是用于数据获取区域的最佳方式</p>
 * <ul>
 * <li>{@link tech.mhuang.pacebox.core.area.Area} 区域类</li>
 * <li>{@link tech.mhuang.pacebox.core.area.AreaUtil} 区域处理类</li>
 * </ul>
 *
 * @author mhuang
 * @since 1.1.2
 */
package tech.mhuang.pacebox.core.area;