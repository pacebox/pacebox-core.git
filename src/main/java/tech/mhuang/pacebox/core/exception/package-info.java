/**
 * 异常处理包
 *
 * <p>主要处理异常的消息包</p>
 * <ul>
 * <li>{@link tech.mhuang.pacebox.core.exception.ExceptionUtil} 异常处理工具类</li>
 * </ul>
 *
 * @author mhuang
 * @since 1.0.0
 */
package tech.mhuang.pacebox.core.exception;