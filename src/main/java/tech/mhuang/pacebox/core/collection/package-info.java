/**
 * 集合工具包.
 *
 * <p>集合工具包</p>
 * <ul>
 * <li>{@link tech.mhuang.pacebox.core.collection.ConcurrentHashSet} concurrentHashSet工具类</li>
 * </ul>
 *
 * @author mhuang
 * @since 1.1.6
 */
package tech.mhuang.pacebox.core.collection;