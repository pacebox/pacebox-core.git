/**
 * 这个包是克隆工具包.
 *
 * <p>{@link tech.mhuang.pacebox.core.clone.BaseCloneable} 克隆接口</p>
 * <p>{@link tech.mhuang.pacebox.core.clone.CloneRuntimeException} 克隆异常包</p>
 * <p>{@link tech.mhuang.pacebox.core.clone.DefaultCloneableSupport} 默认克隆支持</p>
 *
 * @author mhuang
 * @since 1.0.0
 */
package tech.mhuang.pacebox.core.clone;