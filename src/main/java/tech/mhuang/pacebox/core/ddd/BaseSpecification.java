package tech.mhuang.pacebox.core.ddd;

/**
 * 规则器
 *
 * @author mhuang
 * @since 1.1.4
 */
public interface BaseSpecification<T> {

    /**
     * 规则用于实现来操作接口
     *
     * @param t 规则校验得类
     * @return true代表成功、false代表失败
     */
    boolean isSatisifedBy(T t);
}