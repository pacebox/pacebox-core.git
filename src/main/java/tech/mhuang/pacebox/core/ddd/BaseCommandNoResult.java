package tech.mhuang.pacebox.core.ddd;

/**
 * 基础命令没有应答
 *
 * @author mhuang
 * @since 1.0.0
 */
public interface BaseCommandNoResult<Request> {

    void execute(Request request);
}
