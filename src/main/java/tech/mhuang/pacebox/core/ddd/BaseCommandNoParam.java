package tech.mhuang.pacebox.core.ddd;

/**
 * 基础命令没有参数
 *
 * @author mhuang
 * @since 1.0.0
 */
public interface BaseCommandNoParam<Response> {

    Response execute();
}
