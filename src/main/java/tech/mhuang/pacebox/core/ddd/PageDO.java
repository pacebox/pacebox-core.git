package tech.mhuang.pacebox.core.ddd;

/**
 * 分页DO实体类
 *
 * @author mhuang
 * @since 1.1.4
 */
public class PageDO {

    /**
     * 页数
     */
    private Integer rows;

    /**
     * 开始数
     */
    private Integer start;

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }
}
