package tech.mhuang.pacebox.core.ddd;

/**
 * 基础命令
 *
 * @author mhuang
 * @since 1.1.4
 */
public interface BaseCommand<Request,Response> {

    Response execute(Request request);
}

