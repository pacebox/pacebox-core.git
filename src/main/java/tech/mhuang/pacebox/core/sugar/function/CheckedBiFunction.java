package tech.mhuang.pacebox.core.sugar.function;

/**
 * BiFunction
 *
 * @author mhuang
 * @since 1.1.1
 */
@FunctionalInterface
public interface CheckedBiFunction<T, U, R> {

    /**
     * BiFunction#apply
     *
     * @param t t
     * @param u u
     * @return R response
     * @throws Throwable Throwable
     */
    R apply(T t, U u) throws Throwable;

}
