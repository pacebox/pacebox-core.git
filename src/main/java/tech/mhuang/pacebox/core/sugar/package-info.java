/**
 * 语法糖包.
 * <p>用于封装jdk8语法糖处理类</p>
 * <ul>
 *  <li>{@link tech.mhuang.pacebox.core.sugar.Attempt} 语法糖</li>
 * </ul>
 *
 * @author mhuang
 * @since 1.1.0
 */
package tech.mhuang.pacebox.core.sugar;