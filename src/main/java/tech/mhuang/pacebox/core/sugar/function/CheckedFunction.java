package tech.mhuang.pacebox.core.sugar.function;

/**
 * function扩展
 *
 * @author mhuang
 * @since 1.1.1
 */
@FunctionalInterface
public interface CheckedFunction<T, R> {

    /**
     * function#apply
     *
     * @param input input
     * @return r
     * @throws Throwable Throwable
     */
    R apply(T input) throws Throwable;

}
