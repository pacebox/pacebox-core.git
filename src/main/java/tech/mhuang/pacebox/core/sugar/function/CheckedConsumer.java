package tech.mhuang.pacebox.core.sugar.function;

/**
 * Consumer
 *
 * @author mhuang
 * @since 1.1.1
 */
@FunctionalInterface
public interface CheckedConsumer<T> {

    /**
     * Consumer#accpet
     *
     * @param input input
     * @throws Throwable Throwable
     */
    void accept(T input) throws Throwable;

}