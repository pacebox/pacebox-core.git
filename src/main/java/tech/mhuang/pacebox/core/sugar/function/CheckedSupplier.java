package tech.mhuang.pacebox.core.sugar.function;

/**
 * supplier
 *
 * @author mhuang
 * @since 1.1.1
 */
@FunctionalInterface
public interface CheckedSupplier<R> {

    R supply() throws Throwable;

}