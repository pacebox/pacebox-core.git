package tech.mhuang.pacebox.core.sugar.function;

/**
 * BiConsumer 扩展
 *
 * @author mhuang
 * @since 1.1.1
 */
@FunctionalInterface
public interface CheckedBiConsumer<T, U> {

    /**
     * BiConsumer#accpet
     *
     * @param t t
     * @param u u
     * @throws Throwable Throwable
     */
    void accept(T t, U u) throws Throwable;

}
