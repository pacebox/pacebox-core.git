package tech.mhuang.pacebox.core.convert.register;

import tech.mhuang.pacebox.core.convert.AbstractConverter;

import java.math.BigDecimal;

/**
 * bigDecimal转换器
 *
 * @author mhuang
 * @since 1.0.0
 */
public class BigDecimalConverter extends AbstractConverter<BigDecimal> {


    @Override
    public BigDecimal convert(Object source) {
        if (source instanceof BigDecimal) {
            return (BigDecimal) source;
        }
        throw new IllegalArgumentException("原类型无法转换成BigDecimal类型");
    }
}
