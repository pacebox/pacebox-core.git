package tech.mhuang.pacebox.core.convert.register;

import tech.mhuang.pacebox.core.convert.AbstractConverter;

/**
 * Integer 转换器
 *
 * @author mhuang
 * @since 1.0.0
 */
public class LongConverter extends AbstractConverter<Long> {

    @Override
    public Long convert(Object source) {
        if (source instanceof Long || source.getClass() == long.class) {
            return (Long) source;
        }
        throw new IllegalArgumentException("原类型无法转换成Integer类型");
    }
}
